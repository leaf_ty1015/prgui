
// 过滤卡片
function filterCard() {
    var selected = $(".site-tab-selected")
    selectedTag = selected.text().trim() // trim 用于删除目标字符串前后的空格等空白字符
    var cards = $("article.post-card")
    var transitionTime = 400 // 卡片变化的过渡时间，单位ms
    // 遍历所有的cards
    for (let index = 0; index < cards.length; index++) {
        const card = cards[index];
        // 若一个tag都没有被选中则显示所有tab，跳过筛选card的逻辑，直接将所有的card设置为可见
        if (selected.length == 0) {
            $(card).show({
                duration: transitionTime
            })
            continue
        }
        // 找到此card下的所有keywords，在card下有一个li列表
        var tags = $(card).find("li")
        var hide = true
        for (let j = 0; j < tags.length; j++) {
            const tag = tags[j];
            // 至少有一个匹配则将这个card设置为可见，即hide=false
            if (tag.innerText.trim() == selectedTag) {
                hide = false
                break
            }
        }
        // 设置card的可见属性
        if (hide) {
            $(card).hide({
                duration: transitionTime
            })
        } else {
            $(card).show({
                duration: transitionTime
            })
        }
    }
}

// 监听tab点击事件
$(".site-tab").on("click", event => {
    var tab = $(event.target)
    // tab样式的变更
    if (!tab.hasClass("site-tab-selected")) {
        $(".site-tab").removeClass("site-tab-selected")
        tab.addClass("site-tab-selected")
    } else {
        tab.removeClass("site-tab-selected")
    }
    // 卡片的过滤和筛选
    filterCard()
})